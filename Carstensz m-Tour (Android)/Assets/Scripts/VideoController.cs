﻿using UnityEngine;

public class VideoController : MonoBehaviour
{
    public BacksoundController backsoundController;

    private void Awake()
    {
        backsoundController = GameObject.FindGameObjectWithTag("Backsound").GetComponent<BacksoundController>();
        backsoundController.GetComponent<AudioSource>().mute = true;
    }
    // Start is called before the first frame update
    void Start()
    {
        if (backsoundController == null)
        {
            backsoundController = GameObject.FindGameObjectWithTag("Backsound").GetComponent<BacksoundController>();
            backsoundController.GetComponent<AudioSource>().mute = true;
        }
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            backsoundController.GetComponent<AudioSource>().mute = false;
            SetStyleLoader("Atlas Loader");
            ChangeToScene("Carstensz - Home Menu");
        }
    }

    public void SetStyleLoader(string prefabToLoad)
    {
        AtlasLoadingScreen.prefabName = prefabToLoad;
    }

    public void ChangeToScene(string sceneName)
    {
        AtlasLoadingScreen.LoadScene(sceneName);
    }
}
