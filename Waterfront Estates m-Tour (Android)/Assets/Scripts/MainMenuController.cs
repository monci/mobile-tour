﻿using UnityEngine;

public class MainMenuController : MonoBehaviour {
    [SerializeField]
    private Animator exitAnimator;
    [SerializeField]
    private GameObject panelMenu;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            if (exitAnimator.GetBool("Exit") == false)
            {
                ShowExitConfirmation();

            }
            else
            {
                UnShowExitConfirmation();
            }
        }
    }

    public void SetStyleLoader(string prefabToLoad)
    {
        AtlasLoadingScreen.prefabName = prefabToLoad;
    }

    public void ChangeToScene(string sceneName)
    {
        AtlasLoadingScreen.LoadScene(sceneName);
    }

    public void ShowExitConfirmation()
    {
        gameObject.GetComponent<AudioSource>().Play();
        if (exitAnimator.GetBool("Show") == false)
        {
            exitAnimator.SetBool("Show", true);
            exitAnimator.SetBool("Exit", true);
        }
        else
        {
            exitAnimator.SetBool("Exit", true);
        }
            panelMenu.SetActive(false);
    }

    public void UnShowExitConfirmation()
    {
        gameObject.GetComponent<AudioSource>().Play();
        exitAnimator.SetBool("Show", false);
        exitAnimator.SetBool("Exit", false);
        panelMenu.SetActive(true);
    }

    public void OpenLink(string link)
    {
        gameObject.GetComponent<AudioSource>().Play();
        Application.OpenURL(link);
    }
    public void ExitApplication()
    {
        gameObject.GetComponent<AudioSource>().Play();
        Application.Quit();
    }
}
