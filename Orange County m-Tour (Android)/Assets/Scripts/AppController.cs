﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class AppController : MonoBehaviour {

	public Animator fadingAnimator;
    public Animator showMenuAnimator;
    public GameObject imageGyro;
    public GameObject imageTouch;
    public Text textGyroOrTouch;
    public GameObject optionUp;
    public GameObject optionDown;
    public GameObject cameraPlayer;
    public GameObject[] rooms;

	void Start () {
        if (rooms.Length!=0)
        {
            foreach (var room in rooms)
            {
                room.SetActive(false);
            }
            rooms[0].SetActive(true);
        }
	}
	
	void Update () {
        if (Input.GetKeyDown(KeyCode.Escape)) 
        {
            gameObject.GetComponent<AudioSource>().Play();
            if (showMenuAnimator.GetBool("Show") == false)
            {
                ShowMenu();
            }
            else
            {
                UnShowMenu();
            }
        }
	}

    public void GoToOtherRoom(GameObject activeRoom)
    {
        StartCoroutine(NextRoom(activeRoom));
    }

    IEnumerator NextRoom(GameObject room)
    {
        yield return new WaitForSeconds(.5f);
        foreach (var otherRoom in rooms)
        {
            otherRoom.SetActive(false);
        }
        room.SetActive(true);
        Camera.main.transform.position = room.transform.position;
    }

    public void ChangeSkybox(Material newSkyboxRoom)
    {
        gameObject.GetComponent<AudioSource>().Play();
        StartCoroutine(NextSkybox(newSkyboxRoom));
    }

    IEnumerator NextSkybox(Material roomSkybox)
    {
        fadingAnimator.SetTrigger("FadeOut");
        yield return new WaitForSeconds(.5f);
        RenderSettings.skybox = roomSkybox;
        fadingAnimator.SetTrigger("FadeIn");
    }

    public void ShowMenu()
    {
        gameObject.GetComponent<AudioSource>().Play();
        showMenuAnimator.SetBool("Show", true);
        optionUp.SetActive(false);
        optionDown.SetActive(true);
    }

    public void UnShowMenu()
    {
        gameObject.GetComponent<AudioSource>().Play();
        if (showMenuAnimator.GetBool("Exit") == false)
        {
            showMenuAnimator.SetBool("Show", false);
        }
        else
        {
            showMenuAnimator.SetBool("Exit", false);
            showMenuAnimator.SetBool("Show", false);
        }
        optionUp.SetActive(true);
        optionDown.SetActive(false);
    }

    public void ShowExitDialog()
    {
        gameObject.GetComponent<AudioSource>().Play();
        showMenuAnimator.SetBool("Exit", true);
    }

    public void UnShowExitDialog()
    {
        gameObject.GetComponent<AudioSource>().Play();
        showMenuAnimator.SetBool("Exit", false);
    }

    public void ChangeModeView()
    {
        gameObject.GetComponent<AudioSource>().Play();
        cameraPlayer.GetComponent<GyroController>().enabled = !cameraPlayer.GetComponent<GyroController>().enabled;
        cameraPlayer.GetComponent<TouchController>().enabled = !cameraPlayer.GetComponent<TouchController>().enabled;
        if (imageGyro.activeInHierarchy == false)
        {
            imageGyro.SetActive(true);
            imageTouch.SetActive(false);
            textGyroOrTouch.text = "Gyroscope";
        }
        else
        {
            imageGyro.SetActive(false);
            imageTouch.SetActive(true);
            textGyroOrTouch.text = "Touch";
        }
    }

    public void SetStyleLoader(string prefabToLoad)
    {
        AtlasLoadingScreen.prefabName = prefabToLoad;
    }

    public void ChangeToScene(string sceneName)
    {
        AtlasLoadingScreen.LoadScene(sceneName);
    }

    public void ExitApplication()
    {
        gameObject.GetComponent<AudioSource>().Play();
        Application.Quit();
    }
}
